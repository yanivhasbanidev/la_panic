I believe that the word contribution is a way of life that every individual should inspire to.
As such, I thrive to help make life a little better and easier in any way I can.
If you feel the same, and wish to help in any way possible/ report any issue you may have encountered, feel free
to join me in:
https://gitter.im/pa_panic/community
